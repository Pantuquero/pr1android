package org.pantuquero.dota2.Actividades;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.WallpaperManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.*;
import org.pantuquero.dota2.MainActivity;
import org.pantuquero.dota2.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Pantuquero on 05/12/2014.
 */
public class Preferencias extends Activity implements View.OnClickListener {
    TextView tvMensajeBienvenida;
    CheckBox cbBloquearHeroesPreferencias;
    private int resultadoCargaImagen = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preferencias);

        tvMensajeBienvenida = (TextView) findViewById(R.id.etMensajeBienvenidaPreferencias);
        if(!MainActivity.baseDatos.getMensajeBienvenida().equals("null")){
            tvMensajeBienvenida.setText(MainActivity.baseDatos.getMensajeBienvenida());
        }

        cbBloquearHeroesPreferencias = (CheckBox) findViewById(R.id.cbBloquearHeroesPreferencias);
        cbBloquearHeroesPreferencias.setChecked(MainActivity.baseDatos.getBloquearHeroes());
        cbBloquearHeroesPreferencias.setOnClickListener(this);

        Button btAtras = (Button) findViewById(R.id.btAtrasPreferencias);
        btAtras.setOnClickListener(this);
        Button btBorrarBD = (Button) findViewById(R.id.btVaciarDatosPreferencias);
        btBorrarBD.setOnClickListener(this);
        Button btCambiarFondoPreferencias = (Button) findViewById(R.id.btCambiarFondoPreferencias);
        btCambiarFondoPreferencias.setOnClickListener(this);
    }

    private void Confirmacion(){
        new AlertDialog.Builder(this)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle(R.string.titulo_dialogo_confirmacion_preferencias)
            .setMessage(R.string.mensaje_dialogo_confirmacion_preferencias)
            .setPositiveButton(R.string.borrar_dialogo_confirmacion_preferencias, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    MainActivity.mp.start();
                    MainActivity.baseDatos.borrarTablas();
                    MainActivity.baseDatos.crearTablas();
                    MainActivity.listaHeroes.clear();
                    MainActivity.listaObjetos.clear();
                    Toast.makeText(getApplicationContext(), R.string.mensaje_datos_borrados, Toast.LENGTH_SHORT).show();
                }
            }).setNegativeButton(R.string.cancelar_dialogo_confirmacion_preferencias, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    MainActivity.mp.start();
                }
        }).show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btVaciarDatosPreferencias:
                MainActivity.mp.start();
                Confirmacion();
                break;
            case R.id.btAtrasPreferencias:
                MainActivity.mp.start();
                String mensaje = String.valueOf(tvMensajeBienvenida.getText());
                if(mensaje.equals("")){
                    MainActivity.baseDatos.setMensajeBienvenida("null");
                }else{
                    MainActivity.baseDatos.setMensajeBienvenida(mensaje);
                }
                MainActivity.baseDatos.setBloquearHeroes(cbBloquearHeroesPreferencias.isChecked());

                finish();
                break;
            case R.id.cbBloquearHeroesPreferencias:
                MainActivity.mp.start();
                break;
            case R.id.btCambiarFondoPreferencias:
                MainActivity.mp.start();
                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(intent, resultadoCargaImagen);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if((requestCode == resultadoCargaImagen) && (resultCode == RESULT_OK) && (data != null)){
            WallpaperManager wallpaper = WallpaperManager.getInstance(getApplicationContext());
            Uri imagen = data.getData();
            String[] ruta = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(imagen, ruta, null, null, null);
            cursor.moveToFirst();

            int indice = cursor.getColumnIndex(ruta[0]);
            String rutaImagen = cursor.getString(indice);
            cursor.close();

            try {
                wallpaper.setBitmap(BitmapFactory.decodeFile(rutaImagen));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
