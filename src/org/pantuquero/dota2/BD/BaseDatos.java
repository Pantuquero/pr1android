package org.pantuquero.dota2.BD;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import org.pantuquero.dota2.MainActivity;
import org.pantuquero.dota2.R;
import org.pantuquero.dota2.base.Heroe;
import org.pantuquero.dota2.base.Objeto;

import java.util.ArrayList;

/**
 * Created by Pantuquero on 03/12/2014.
 */
public class BaseDatos extends SQLiteOpenHelper {

    public BaseDatos(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public void crearTablas (){
        SQLiteDatabase db = this.getReadableDatabase();

        db.execSQL("CREATE TABLE IF NOT EXISTS heroes(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "nombre VARCHAR(20)," +
                "inteligencia VARCHAR(20)," +
                "fuerza VARCHAR(20)," +
                "agilidad VARCHAR(20));"
        );

        db.execSQL("CREATE TABLE IF NOT EXISTS objetos(" +
                "id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "nombre VARCHAR(20)," +
                "definicion VARCHAR(500)," +
                "efecto VARCHAR(50));"
        );

        rellenarTablas();

        db.execSQL("CREATE TABLE IF NOT EXISTS mensaje_bienvenida(" +
                "mensaje VARCHAR(20));");
        comprobarMensjeBienvenida();

        db.execSQL("CREATE TABLE IF NOT EXISTS bloquear_heroes(" +
                "bloquear INTEGER(1));");
        comprobarBloquearHeroes();

        db.execSQL("CREATE TABLE IF NOT EXISTS ruta_wallpaper(" +
                "ruta VARCHAR(100));");
    }

    private void rellenarHeroes (SQLiteDatabase db){
        ContentValues values = new ContentValues();

        values.put("nombre", "Axe");
        values.put("inteligencia", "18");
        values.put("agilidad", "20");
        values.put("fuerza", "25");
        db.insertOrThrow("heroes", null, values);

        values.clear();
        values.put("nombre", "Dragon Knight");
        values.put("inteligencia", "15");
        values.put("agilidad", "19");
        values.put("fuerza", "19");
        db.insertOrThrow("heroes", null, values);

        values.clear();
        values.put("nombre", "Sniper");
        values.put("inteligencia", "15");
        values.put("agilidad", "21");
        values.put("fuerza", "16");
        db.insertOrThrow("heroes", null, values);

        values.clear();
        values.put("nombre", "Rikimaru");
        values.put("inteligencia", "14");
        values.put("agilidad", "34");
        values.put("fuerza", "17");
        db.insertOrThrow("heroes", null, values);

        values.clear();
        values.put("nombre", "Bane Elemental");
        values.put("inteligencia", "22");
        values.put("agilidad", "22");
        values.put("fuerza", "22");
        db.insertOrThrow("heroes", null, values);
    }

    private void rellenarObjetos(SQLiteDatabase db){
        ContentValues values = new ContentValues();

        values.put("nombre", "Malla de cadenas");
        values.put("definicion", "Una armadura media de cadenas enlazadas");
        values.put("efecto", "+5 Armadura");
        db.insertOrThrow("objetos", null, values);

        values.clear();
        values.put("nombre", "Varita magica");
        values.put("definicion", "Una simple varita para canalizar energías magicas");
        values.put("efecto", "+3 A todos los atributos");
        db.insertOrThrow("objetos", null, values);

        values.clear();
        values.put("nombre", "Vanguardia");
        values.put("definicion", "Un poderoso escudo que defiende a su portador incluso de los ataques mas duros");
        values.put("efecto", "+250 Vida, +6 regeneracion de vida");
        db.insertOrThrow("objetos", null, values);

        values.clear();
        values.put("nombre", "Furia de batalla");
        values.put("definicion", "Esta poderosa hacha es capaz de cortar oleadas de enemigos de un solo golpe");
        values.put("efecto", "+65 Daño, +6 regeneracion de vida, 150% de regeneracion de mana");
        db.insertOrThrow("objetos", null, values);

        values.clear();
        values.put("nombre", "Ojo de Skadi");
        values.put("definicion", "Un objeto extremadamente dificil de encontrar, custodiado por los dragones Azures");
        values.put("efecto", "+25 A todos los atributos, +250 Vida, +250 Mana");
        db.insertOrThrow("objetos", null, values);
    }

    private void rellenarTablas(){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM heroes", null);
        cursor.moveToFirst();
        int cantidad = cursor.getInt(0);

        if(cantidad==0){
            rellenarHeroes(db);
        }

        cursor = db.rawQuery("SELECT COUNT (*) FROM objetos", null);
        cursor.moveToFirst();
        cantidad = cursor.getInt(0);
        if(cantidad==0){
            rellenarObjetos(db);
        }
    }


    @Override
    public void onCreate(SQLiteDatabase db) {


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public ArrayList<Heroe> getHeroes(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Heroe> lista = new ArrayList<Heroe>();
        Heroe heroe;
        Cursor cursor;

        cursor = db.rawQuery("SELECT * FROM heroes", null);

        while (cursor.moveToNext()){
            heroe = new Heroe();

            heroe.setId(cursor.getInt(0));
            heroe.setNombre(cursor.getString(1));
            heroe.setInteligencia(cursor.getInt(2));
            heroe.setAgilidad(cursor.getInt(3));
            heroe.setFuerza(cursor.getInt(4));

            lista.add(heroe);
        }

        return lista;
    }

    public ArrayList<Objeto> getObjetos(){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<Objeto> lista = new ArrayList<Objeto>();
        Objeto objeto;
        Cursor cursor;

        cursor = db.rawQuery("SELECT * FROM objetos", null);

        while (cursor.moveToNext()){
            objeto = new Objeto();

            objeto.setId(cursor.getInt(0));
            objeto.setNombre(cursor.getString(1));
            objeto.setDefinicion(cursor.getString(2));
            objeto.setEfecto(cursor.getString(3));

            lista.add(objeto);
        }

        return lista;
    }

    public int nuevoHeroe(Heroe heroe){
        SQLiteDatabase db = this.getReadableDatabase();

        ContentValues values = new ContentValues();
        values.put("nombre", heroe.getNombre());
        values.put("inteligencia", heroe.getInteligencia());
        values.put("agilidad", heroe.getAgilidad());
        values.put("fuerza", heroe.getFuerza());
        db.insertOrThrow("heroes", null, values);

        Cursor cursor = db.rawQuery("SELECT id FROM heroes WHERE nombre = '"+heroe.getNombre()+"';", null);
        cursor.moveToFirst();
        int id = cursor.getInt(0);
        return id;
    }

    public void eliminarHeroe(Heroe heroe){
        SQLiteDatabase db = this.getReadableDatabase();

        db.execSQL("DELETE FROM heroes WHERE id = '"+ heroe.getId() +"';");
    }

    public void editarHeroe(Heroe heroe){
        SQLiteDatabase db = this.getReadableDatabase();

        db.execSQL("UPDATE heroes SET nombre = '" + heroe.getNombre() + "', inteligencia = '" + heroe.getInteligencia() + "', agilidad = '" + heroe.getAgilidad() + "', fuerza = '" + heroe.getFuerza() + "' WHERE id = '" + heroe.getId() + "'");
    }

    public void borrarTablas(){
        SQLiteDatabase db = this.getReadableDatabase();

        db.execSQL("DROP TABLE IF EXISTS heroes");
        db.execSQL("DROP TABLE IF EXISTS objetos");
    }

    private void comprobarMensjeBienvenida(){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM mensaje_bienvenida;", null);
        cursor.moveToFirst();
        int vacio = cursor.getInt(0);
        if(vacio==0){
            ContentValues values = new ContentValues();

            values.put("mensaje", "null");
            db.insertOrThrow("mensaje_bienvenida", null, values);
        }
    }

    public void setMensajeBienvenida(String mensaje){
        SQLiteDatabase db = this.getReadableDatabase();

        db.execSQL("UPDATE mensaje_bienvenida SET mensaje = '" + mensaje + "';");
    }

    public String getMensajeBienvenida(){
        SQLiteDatabase db = this.getReadableDatabase();
        String mensaje = "vacio";

        Cursor cursor = db.rawQuery("SELECT mensaje FROM mensaje_bienvenida;", null);
        cursor.moveToFirst();
        mensaje = cursor.getString(0);

        return mensaje;
    }

    private void comprobarBloquearHeroes(){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM bloquear_heroes;", null);
        cursor.moveToFirst();
        int vacio = cursor.getInt(0);
        if(vacio==0){
            ContentValues values = new ContentValues();

            values.put("bloquear", 0);
            db.insertOrThrow("bloquear_heroes", null, values);
        }
    }

    public void setBloquearHeroes(boolean bloquear){
        SQLiteDatabase db = this.getReadableDatabase();
        int bloq = 0;

        if(bloquear==true){
            bloq = 1;
        }

        db.execSQL("UPDATE bloquear_heroes SET bloquear = '" + bloq + "';");
    }

    public boolean getBloquearHeroes(){
        SQLiteDatabase db = this.getReadableDatabase();
        int bloq = 0;
        boolean bloquear = false;

        Cursor cursor = db.rawQuery("SELECT bloquear FROM bloquear_heroes;", null);
        cursor.moveToFirst();
        bloq = cursor.getInt(0);

        if(bloq==1){
            bloquear=true;
        }

        return bloquear;
    }

    public void setRutaWallpaper(String ruta){
        SQLiteDatabase db = this.getReadableDatabase();

        db.execSQL("UPDATE ruta_wallpaper SET ruta = '"+ruta+"'");
    }

    public String getRutaWallpaper(){
        SQLiteDatabase db = this.getReadableDatabase();
        String ruta = null;

        Cursor cursor = db.rawQuery("SELECT ruta FROM ruta_wallpaper;", null);
        cursor.moveToFirst();
        if(cursor.getCount()!=0)
            ruta = cursor.getString(0);

        return ruta;
    }
}
