package org.pantuquero.dota2;

import android.app.*;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import org.pantuquero.dota2.Actividades.Preferencias;
import org.pantuquero.dota2.BD.BaseDatos;
import org.pantuquero.dota2.Fragmentos.ListaHeroes;
import org.pantuquero.dota2.Fragmentos.ListaObjetos;
import org.pantuquero.dota2.Fragmentos.Mapa;
import org.pantuquero.dota2.Listeners.TabListener;
import org.pantuquero.dota2.base.Heroe;
import org.pantuquero.dota2.base.Objeto;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

public class MainActivity extends Activity implements View.OnCreateContextMenuListener {
    public static BaseDatos baseDatos;
    public static ArrayList<Heroe> listaHeroes = new ArrayList<Heroe>();
    public static ArrayList<Objeto> listaObjetos = new ArrayList<Objeto>();
    public static MediaPlayer mp;
    private MediaPlayer welcome;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        welcome = MediaPlayer.create(this, R.raw.welcome);
        mp = MediaPlayer.create(this, R.raw.click);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        baseDatos = new BaseDatos(this, "basedatos", null, 1);
        baseDatos.crearTablas();
        listaHeroes = baseDatos.getHeroes();
        listaObjetos = baseDatos.getObjetos();

        cargarPestanas();
        mostrarMensajeBienvenida();
    }

    private void cargarPestanas(){
        Resources recursos = getResources();

        ActionBar actionBar = getActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        ActionBar.Tab pestanaHeroes = actionBar.newTab().setText(R.string.pestana_heroes_main);
        ActionBar.Tab pestanaObjetos = actionBar.newTab().setText(R.string.pestana_objetos_main);
        ActionBar.Tab pestanaMaps = actionBar.newTab().setText(R.string.pestana_mapa_main);

        Fragment fragmentoHeroes = new ListaHeroes();
        Fragment fragmentoObjetos = new ListaObjetos();
        Fragment fragmentoMapa = new Mapa();

        pestanaHeroes.setTabListener(new TabListener(fragmentoHeroes));
        pestanaObjetos.setTabListener(new TabListener(fragmentoObjetos));
        pestanaMaps.setTabListener(new TabListener(fragmentoMapa));

        actionBar.addTab(pestanaHeroes);
        actionBar.addTab(pestanaObjetos);
        actionBar.addTab(pestanaMaps);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_opciones, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = null;

        switch(item.getItemId()) {
            case R.id.menu_preferencias:
                mp.start();
                intent = new Intent(this, Preferencias.class);
                startActivity(intent);
                break;
            case R.id.menu_acerca_de:
                mp.start();
                mostrarAcercaDe();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
        return true;
    }

    protected void mostrarAcercaDe() {
        View messageView = getLayoutInflater().inflate(R.layout.acerca_de, null, false);

        TextView textView = (TextView) messageView.findViewById(R.id.about_credits);
        textView.setTextColor(Color.WHITE);
        TextView textView1 = (TextView) messageView.findViewById(R.id.about_description);
        textView1.setTextColor(Color.WHITE);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setIcon(R.drawable.dota2_logo_chiquitin);
        builder.setTitle(R.string.app_name);
        builder.setView(messageView);
        builder.create();
        builder.show();
    }

    private void mostrarMensajeBienvenida(){
        if(baseDatos.getMensajeBienvenida().equals("null")){
            return;
        }else{
            new AlertDialog.Builder(this)
                    .setIcon(android.R.drawable.ic_dialog_info)
                    .setTitle(R.string.titulo_mensaje_bienvenida)
                    .setMessage(baseDatos.getMensajeBienvenida())
                    .setPositiveButton(R.string.aceptar_mensaje_bienvenida, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).show();
            welcome.start();
        }
    }
}
