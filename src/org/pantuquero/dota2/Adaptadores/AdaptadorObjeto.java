package org.pantuquero.dota2.Adaptadores;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import org.pantuquero.dota2.R;
import org.pantuquero.dota2.base.Heroe;
import org.pantuquero.dota2.base.Objeto;

import java.util.ArrayList;

/**
 * Created by Pantuquero on 03/12/2014.
 */
public class AdaptadorObjeto extends BaseAdapter {
    private ArrayList<Objeto> listaObjetos;
    private LayoutInflater inflador;

    public AdaptadorObjeto(Activity contexto, ArrayList<Objeto> lista){
        this.listaObjetos = lista;
        this.inflador = LayoutInflater.from(contexto);
    }

    static class ViewHolder {
        ImageView imagen;
        TextView nombre;
    }

    @Override
    public int getCount() {
        return this.listaObjetos.size();
    }

    @Override
    public Object getItem(int position) {
        return this.listaObjetos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null){
            convertView = inflador.inflate(R.layout.fila, null);

            holder = new ViewHolder();
            holder.imagen = (ImageView) convertView.findViewById(R.id.ivImagen);
            holder.nombre = (TextView) convertView.findViewById(R.id.tvNombre);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }

        Objeto objeto = listaObjetos.get(position);
        holder.imagen.setImageResource(R.drawable.objeto);
        holder.nombre.setText(objeto.getNombre());

        return convertView;
    }

}
