package org.pantuquero.dota2.Adaptadores;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import org.pantuquero.dota2.MainActivity;
import org.pantuquero.dota2.R;
import org.pantuquero.dota2.base.Heroe;
import org.w3c.dom.Text;

import java.util.ArrayList;

/**
 * Created by Pantuquero on 03/12/2014.
 */
public class AdaptadorHeroe extends BaseAdapter {
    private ArrayList<Heroe> listaHeroes;
    private LayoutInflater inflador;

    public AdaptadorHeroe(Activity contexto, ArrayList<Heroe> lista){
        this.listaHeroes = lista;
        this.inflador = LayoutInflater.from(contexto);
    }

    static class ViewHolder {
        ImageView imagen;
        TextView nombre;
    }

    @Override
    public int getCount() {
        return this.listaHeroes.size();
    }

    @Override
    public Object getItem(int position) {
        return this.listaHeroes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if(convertView == null){
            convertView = inflador.inflate(R.layout.fila, null);

            holder = new ViewHolder();
            holder.imagen = (ImageView) convertView.findViewById(R.id.ivImagen);
            holder.nombre = (TextView) convertView.findViewById(R.id.tvNombre);

            convertView.setTag(holder);
        }else{
            holder = (ViewHolder) convertView.getTag();
        }


        Heroe heroe = MainActivity.listaHeroes.get(position);
        holder.imagen.setImageResource(R.drawable.heroe);
        holder.nombre.setText(heroe.getNombre());

        return convertView;
    }
}
