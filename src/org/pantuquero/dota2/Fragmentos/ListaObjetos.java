package org.pantuquero.dota2.Fragmentos;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import org.pantuquero.dota2.Actividades.VerObjeto;
import org.pantuquero.dota2.Adaptadores.AdaptadorObjeto;
import org.pantuquero.dota2.MainActivity;
import org.pantuquero.dota2.R;

/**
 * Created by Pantuquero on 04/12/2014.
 */
public class ListaObjetos extends Fragment {
    private AdaptadorObjeto adaptador;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lista_objetos, container, false);

        adaptador = new AdaptadorObjeto(getActivity(), MainActivity.listaObjetos);
        ListView lvLista = (ListView) view.findViewById(R.id.lvObjetos);
        lvLista.setAdapter(adaptador);

        lvLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MainActivity.mp.start();
                Intent intent = new Intent(getActivity(), VerObjeto.class);

                Bundle paquete = new Bundle();
                paquete.putInt(null, position);
                intent.putExtras(paquete);

                startActivity(intent);
            }
        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        adaptador.notifyDataSetChanged();
    }
}
