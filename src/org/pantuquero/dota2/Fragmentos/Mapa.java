package org.pantuquero.dota2.Fragmentos;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.MarkerOptions;
import org.pantuquero.dota2.MainActivity;
import org.pantuquero.dota2.R;

/**
 * Created by Pantuquero on 09/12/2014.
 */
public class Mapa extends Fragment{
    private GoogleMap mapa;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.mapa, null, false);

        MapsInitializer.initialize(getActivity());

        Toast.makeText(getActivity(), R.string.mensaje_inicio_maps, Toast.LENGTH_LONG).show();

        mapa = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        ubicacionInicial();
        anadirMarcadores();

        return v;
    }

    private void ubicacionInicial(){
        CameraUpdate camara = CameraUpdateFactory.newLatLng(new LatLng(41.648822, -0.889085));
        mapa.moveCamera(camara);
        mapa.animateCamera(CameraUpdateFactory.zoomTo(13.0f), 1500, null);
    }

    private void anadirMarcadores(){
        mapa.addMarker(new MarkerOptions().position(new LatLng(41.637628, -0.901478)).title("Auditorio"));
        mapa.addMarker(new MarkerOptions().position(new LatLng(41.656441, -0.878738)).title("Plaza del pilar"));
        mapa.addMarker(new MarkerOptions().position(new LatLng(41.642187, -0.877944)).title("Parque Miraflores"));
    }

}
