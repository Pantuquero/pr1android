package org.pantuquero.dota2.Fragmentos;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Bundle;
import android.view.*;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import org.pantuquero.dota2.Actividades.NuevoHeroe;
import org.pantuquero.dota2.Actividades.VerHeroe;
import org.pantuquero.dota2.Adaptadores.AdaptadorHeroe;
import org.pantuquero.dota2.MainActivity;
import org.pantuquero.dota2.R;
import org.pantuquero.dota2.base.Heroe;

/**
 * Created by Pantuquero on 03/12/2014.
 */
public class ListaHeroes extends Fragment implements View.OnClickListener, View.OnCreateContextMenuListener {
    private AdaptadorHeroe adaptador;
    Button btNuevo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lista_heroes, container, false);

        adaptador = new AdaptadorHeroe(getActivity(), MainActivity.listaHeroes);
        ListView lvLista = (ListView) view.findViewById(R.id.lvHeroes);
        lvLista.setAdapter(adaptador);
        lvLista.setOnCreateContextMenuListener(this);

        lvLista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                MainActivity.mp.start();
                Intent intent = new Intent(getActivity(), VerHeroe.class);

                Bundle paquete = new Bundle();
                paquete.putInt(null, position);
                intent.putExtras(paquete);

                startActivity(intent);
            }
        });

        btNuevo =(Button) view.findViewById(R.id.btNuevoHeroe);
        btNuevo.setOnClickListener(this);
        btNuevo.setEnabled(MainActivity.baseDatos.getBloquearHeroes());

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        adaptador.notifyDataSetChanged();
        btNuevo.setEnabled(MainActivity.baseDatos.getBloquearHeroes());
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btNuevoHeroe:
                MainActivity.mp.start();
                Intent intent = new Intent(getActivity(), NuevoHeroe.class);

                Bundle paquete = new Bundle();
                paquete.putString("accion", "nuevo");
                paquete.putInt("posicion", -1);
                intent.putExtras(paquete);

                startActivity(intent);
                break;
            default:
                break;
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getActivity().getMenuInflater().inflate(R.menu.menu_contextual, menu);
    }

    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        Heroe heroe = null;
        Intent intent = null;

        if(MainActivity.baseDatos.getBloquearHeroes()){
            switch(item.getItemId()){
                case R.id.menu_eliminar:
                    MainActivity.mp.start();
                    Confirmacion(info);
                    return true;
                case R.id.menu_editar:
                    MainActivity.mp.start();
                    intent = new Intent(getActivity(), NuevoHeroe.class);

                    Bundle paquete = new Bundle();
                    paquete.putString("accion", "editar");
                    paquete.putInt("posicion", info.position);
                    intent.putExtras(paquete);

                    startActivity(intent);
                    return true;
                default:
                    return true;
            }
        }else{
            Toast.makeText(getActivity(), R.string.mensaje_no_tienes_permiso, Toast.LENGTH_LONG).show();
        }
        return super.onContextItemSelected(item);
    }

    public void eliminarHeroe(AdapterView.AdapterContextMenuInfo info){
        Heroe heroe = MainActivity.listaHeroes.get(info.position);
        MainActivity.listaHeroes.remove(info.position);
        MainActivity.baseDatos.eliminarHeroe(heroe);
        adaptador.notifyDataSetChanged();
    }

    private void Confirmacion(AdapterView.AdapterContextMenuInfo inf ){
        final AdapterView.AdapterContextMenuInfo info = inf;
        new AlertDialog.Builder(getActivity())
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle(R.string.titulo_dialogo_confirmacion_borrar_heroe)
                .setMessage(R.string.mensaje_dialogo_confirmacion_borrar_heroe)
                .setPositiveButton(R.string.borrar_dialogo_confirmacion_borrar_heroe, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.mp.start();
                        eliminarHeroe(info);
                        Toast.makeText(getActivity(), R.string.mensaje_heroe_eliminado, Toast.LENGTH_SHORT).show();
                    }
                }).setNegativeButton(R.string.cancelar_dialogo_confirmacion_borrar_heroe, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        MainActivity.mp.start();
                    }
        }).show();

    }
}
