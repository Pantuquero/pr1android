package org.pantuquero.dota2.Actividades;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import org.pantuquero.dota2.MainActivity;
import org.pantuquero.dota2.R;
import org.pantuquero.dota2.base.Heroe;

/**
 * Created by Pantuquero on 04/12/2014.
 */
public class VerHeroe extends Activity implements View.OnClickListener{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ver_heroe);

        Button btAtras = (Button) findViewById(R.id.btAtras);
        btAtras.setOnClickListener(this);

        EditText nombreHeroe = (EditText) findViewById(R.id.etNombreHeroe);
        EditText inteligenciaHeroe = (EditText) findViewById(R.id.etInteligenciaHeroe);
        EditText agilidadHeroe = (EditText) findViewById(R.id.etAgilidadHeroe);
        EditText fuerzaHeroe = (EditText) findViewById(R.id.etFuerzaHeroe);


        Bundle paquete = getIntent().getExtras();
        int posicion = paquete.getInt(null);

        Heroe heroe = MainActivity.listaHeroes.get(posicion);
        nombreHeroe.setText(heroe.getNombre());
        inteligenciaHeroe.setText(String.valueOf(heroe.getInteligencia()));
        agilidadHeroe.setText(String.valueOf(heroe.getAgilidad()));
        fuerzaHeroe.setText(String.valueOf(heroe.getFuerza()));
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btAtras:
                MainActivity.mp.start();
                finish();
                break;
        }
    }
}
