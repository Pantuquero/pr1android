package org.pantuquero.dota2.Listeners;

import android.app.ActionBar;
import android.app.Fragment;
import android.app.FragmentTransaction;
import org.pantuquero.dota2.MainActivity;
import org.pantuquero.dota2.R;

/**
 * Created by Pantuquero on 03/12/2014.
 */
public class TabListener implements ActionBar.TabListener {
    private Fragment fragment = null;

    public TabListener (Fragment frag){
        this.fragment = frag;
    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        MainActivity.mp.start();
        ft.replace(R.id.container, fragment);
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
        ft.remove(fragment);
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
        MainActivity.mp.start();
    }
}
