package org.pantuquero.dota2.Actividades;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import org.pantuquero.dota2.MainActivity;
import org.pantuquero.dota2.R;
import org.pantuquero.dota2.base.Objeto;

/**
 * Created by Pantuquero on 04/12/2014.
 */
public class VerObjeto extends Activity implements View.OnClickListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.ver_objeto);

        Button btAtras = (Button) findViewById(R.id.btAtras);
        btAtras.setOnClickListener(this);

        EditText nombreObjeto = (EditText) findViewById(R.id.etNombreObjeto);
        EditText definicionObjeto = (EditText) findViewById(R.id.etDefinicionObjeto);
        EditText efectoObjeto = (EditText) findViewById(R.id.etEfectoObjeto);

        Bundle paquete = getIntent().getExtras();
        int posicion = paquete.getInt(null);

        Objeto objeto = MainActivity.listaObjetos.get(posicion);
        nombreObjeto.setText(objeto.getNombre());
        definicionObjeto.setText(objeto.getDefinicion());
        efectoObjeto.setText(objeto.getEfecto());
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btAtras:
                MainActivity.mp.start();
                finish();
                break;
        }
    }
}
