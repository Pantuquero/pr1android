package org.pantuquero.dota2.Actividades;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import org.pantuquero.dota2.MainActivity;
import org.pantuquero.dota2.R;
import org.pantuquero.dota2.base.Heroe;

/**
 * Created by Pantuquero on 04/12/2014.
 */
public class NuevoHeroe extends Activity implements View.OnClickListener {
    private String accion;
    private int posicion;

    private int idHeroe;

    EditText etNombreHeroe;
    EditText etInteligenciaHeroe;
    EditText etAgilidadHeroe;
    EditText etFuerzaHeroe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.nuevo_heroe);

        etNombreHeroe = (EditText) findViewById(R.id.etNombreHeroe);
        etInteligenciaHeroe = (EditText) findViewById(R.id.etInteligenciaHeroe);
        etAgilidadHeroe = (EditText) findViewById(R.id.etAgilidadHeroe);
        etFuerzaHeroe = (EditText) findViewById(R.id.etFuerzaHeroe);

        Bundle bundle = getIntent().getExtras();
        this.accion = bundle.getString("accion");
        if(this.accion.equalsIgnoreCase("editar")){
            posicion = bundle.getInt("posicion");
            cargarHeroe();
        }

        Button btNuevo =(Button) findViewById(R.id.btNuevoHeroe);
        btNuevo.setOnClickListener(this);
        Button btAtras = (Button) findViewById(R.id.btAtras);
        btAtras.setOnClickListener(this);
    }

    private void cargarHeroe() {
        Heroe heroe = MainActivity.listaHeroes.get(posicion);
        idHeroe = heroe.getId();
        etNombreHeroe.setText(heroe.getNombre());
        etInteligenciaHeroe.setText(String.valueOf(heroe.getInteligencia()));
        etAgilidadHeroe.setText(String.valueOf(heroe.getAgilidad()));
        etFuerzaHeroe.setText(String.valueOf(heroe.getFuerza()));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btNuevoHeroe:
                MainActivity.mp.start();
                Heroe heroe = new Heroe();

                String cadena = String.valueOf(etNombreHeroe.getText());
                if(cadena.equals("")){
                    Toast.makeText(getApplicationContext(), R.string.mensaje_campo_vacio, Toast.LENGTH_SHORT).show();
                    break;
                }
                heroe.setNombre(String.valueOf(etNombreHeroe.getText()));

                try{
                    cadena = String.valueOf(etInteligenciaHeroe.getText());
                    if(cadena.equals("")){
                        Toast.makeText(getApplicationContext(), R.string.mensaje_campo_vacio, Toast.LENGTH_SHORT).show();
                        break;
                    }
                    heroe.setInteligencia(Integer.parseInt(String.valueOf(etInteligenciaHeroe.getText())));

                    cadena = String.valueOf(etAgilidadHeroe.getText());
                    if(cadena.equals("")){
                        Toast.makeText(getApplicationContext(), R.string.mensaje_campo_vacio, Toast.LENGTH_SHORT).show();
                        break;
                    }
                    heroe.setAgilidad(Integer.parseInt(String.valueOf(etAgilidadHeroe.getText())));

                    cadena = String.valueOf(etFuerzaHeroe.getText());
                    if(cadena.equals("")){
                        Toast.makeText(getApplicationContext(), R.string.mensaje_campo_vacio, Toast.LENGTH_SHORT).show();
                        break;
                    }
                    heroe.setFuerza(Integer.parseInt(String.valueOf(etFuerzaHeroe.getText())));
                }catch (NumberFormatException e){
                    Toast.makeText(getApplicationContext(), R.string.mensaje_formato_datos_incorrecto, Toast.LENGTH_SHORT).show();
                    break;
                }

                if(accion.equalsIgnoreCase("nuevo")){
                    int id = MainActivity.baseDatos.nuevoHeroe(heroe);
                    heroe.setId(id);
                    MainActivity.listaHeroes.add(heroe);
                    Toast.makeText(getApplicationContext(), R.string.mensaje_nuevo_heroe, Toast.LENGTH_SHORT).show();
                }else{
                    heroe.setId(idHeroe);
                    MainActivity.listaHeroes.set(posicion, heroe);
                    MainActivity.baseDatos.editarHeroe(heroe);
                    Toast.makeText(getApplicationContext(), R.string.mensaje_heroe_editado, Toast.LENGTH_SHORT).show();
                }
                finish();


                break;
            case R.id.btAtras:
                MainActivity.mp.start();
                finish();
                break;
        }
    }
}
