package org.pantuquero.dota2.base;

import android.graphics.Bitmap;

/**
 * Created by Pantuquero on 03/12/2014.
 */
public class Objeto {
    private int id;
    private String nombre;
    private String definicion;
    private String efecto;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDefinicion() {
        return definicion;
    }

    public void setDefinicion(String definicion) {
        this.definicion = definicion;
    }

    public String getEfecto() {
        return efecto;
    }

    public void setEfecto(String efecto) {
        this.efecto = efecto;
    }
}
